---
title: "File Generators"
date: 2020-03-21T16:13:46.370Z
draft: false
TableOfContents: true
weight: 50
---

To make it easy for you to create files using templates, there are easy commands that you can use.

We are using a file generator called [Plop](https://plopjs.com/documentation/).

To run a list of file generators that is accessible in this project, just run:

```bash
npm run plop
```

or

```bash
npx plop
```

Note: make sure that npx is installed by running

```bash
npm i -g npx
```
