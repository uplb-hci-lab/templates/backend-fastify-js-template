# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/uplb-hci-lab/templates/base-template/compare/v0.0.2...v0.0.3) (2020-03-22)


### Features

* add sentence-splitter ([25d7f3e](https://gitlab.com/uplb-hci-lab/templates/base-template/commit/25d7f3e4c463c91e07287fa51fc10b3baaaf1048))


### Bug Fixes

* add text splitter on description on add-file ([5e869e6](https://gitlab.com/uplb-hci-lab/templates/base-template/commit/5e869e65c45946606d1e8e422fe2c6ddac3ceb6f))

### 0.0.2 (2020-03-21)
